<!DOCTYPE html>
<html lang="<?= $language->language ?>" dir="<?= $language->dir ?>" >
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <?= $head ?>
  <title><?= $head_title ?></title>
  <?= $styles ?>
  <?= $scripts ?>

  <!-- Facebook open graph configuration -->
  <meta property="og:title" content="<?= $head_title ?>">
  <meta property="og:type" content="website">
  <meta property="og:url" content="https://kde.org/">
  <meta property="og:description" content="KDE is an open community of friendly people who want to create a world in which everyone has control over their digital life and enjoys freedom and privacy.">
  <meta property="og:site_name" content="KDE.org">
  <meta property="og:image" content="https://kde.org/stuff/clipart/logo/kde-logo-white-blue-rounded-128x128.png">

  <!-- Twitter card configuration -->
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:site" content="@kdecommunity">
  <meta name="twitter:title" content="KDE Community Home">
  <meta name="twitter:description" content="KDE is an open community of friendly people who want to create a world in which everyone has control over their digital life and enjoys freedom and privacy.">
  <meta name="twitter:image" content="https://kde.org/stuff/clipart/logo/kde-logo-white-blue-rounded-128x128.png">

  <!-- schema.org metadata -->
  <script type="text/javascript" async="" defer="" src="https://stats.kde.org/matomo.js"></script><script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "WebSite",
    "url": "https://kde.org/",
    "name": "KDE.org",
    "description": "KDE is an open community of friendly people who want to create a world in which everyone has control over their digital life and enjoys freedom and privacy."
  }
  </script>

  <!-- Icons -->
  <link rel="apple-touch-icon" href="https://kde.org/aether/media/180x180.png" />
  <link rel="shortcut icon" href="https://kde.org/aether/media/192x192.png" />
  <link rel="stylesheet" href="https://cdn.kde.org/aether-devel/bootstrap.css" />
  <link rel="stylesheet" href="https://cdn.kde.org/aether-devel/kde-org/index.css" />
</head>
<body>
  <header id="KGlobalHeader" class="header clearfix">
    <nav class="navbar navbar-expand-lg">
      <a aria-label="KDE Homepage" class="kde-logo navbar-brand active" href="/">Join the Game!</a>
    </nav>
  </header>
  <?= $page_top; ?>
  <?= $page ?>
  <?= $page_bottom ?>
</body>
</html>
